<style>


.footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: gray;
    color: white;
    text-align: center;
}
</style>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarColor01">
          <ul class="navbar-nav mr-auto col-sm-3 col-md-3">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Lorem</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Lorem</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Lorem</a>
            </li>
			<li class="nav-item">
				<div class="dropdown show">
				  <a class="btn nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Dropdown link
				  </a>

				  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
					<a class="dropdown-item" href="#">Lorem</a>
					<a class="dropdown-item" href="#">Lorem ipsum</a>
					<a class="dropdown-item" href="#">Lorem ipsum</a>
				  </div>
				</div>
			</li>
          </ul>
          
        </div>
    </nav>
    <!-- end navbar -->

    <!-- login modal-->
    <div class="modal" id="loginModal1" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Sing Up</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
       <span aria-hidden="true">×</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="row">
        <div class="col-md-12">

          <form class="form" role="form" method="post" action="singup" accept-charset="UTF-8" id="login-nav">
          @csrf 
            <div class="form-group">
              <input type="text" class="form-control" name="name"  placeholder="Name" required>                       
            </div>

            <div class="form-group">
              <input type="text" class="form-control" name="phone" placeholder="Mobile No" required>
            </div>


            <div class="form-group">
              <input type="email" class="form-control" name="email" placeholder="Email address" required>
            </div>

           <div class="form-group">
              <input type="password" class="form-control" name="password" placeholder="Password" required>
            </div>

            <div class="form-group">
              <input type="password" class="form-control" name="confirmed" placeholder="confirmed Password" required>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">Sign up  </button>
            </div>

         </form>
        </div>
      
        <div class="bottom text-center">
        New here ? <a href="#"><b>Join Us</b></a>
        </div>
      </div>
      </div>

       
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>
