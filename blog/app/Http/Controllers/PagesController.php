<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
   public function index(){
	   return view('pages.home');
	  // return "i am index";
   } 
   
   public function about(){
	   return view('pages.about');
   }
   
   public function servies(){
	   return view('pages.servies');
   }
}
