<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user =auth()->user('id');
        
        $posts = \DB::table('posts')
            ->join('users', 'posts.user_id', '=', 'users.id')
            ->select('users.*', 'posts.*')
            ->where('posts.user_id', $user->id)
            ->get();
        return view('home')->with('posts',$posts);
    }
}
