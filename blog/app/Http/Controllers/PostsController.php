<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$posts = Post::orderby('created_at','desc')->get();
        
        $posts = \DB::table('posts')
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->select('users.*', 'posts.*')
            ->orderby('posts.created_at','desc')
            ->paginate(3);

		return view('posts.index')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        if($user =auth()->user('id')){
            return view('posts.create');
        }
       else{
           echo "Login Frist";
          
       }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titel' => 'required|unique:posts',
            'body' => 'required',
        ]);
		$post = new Post;
		$post->titel = $request->input('titel');
		$post->body = $request->input('body');
       // $post->user_id = auth()->id;
        $post->user_id = Auth()->user()->id;
    	$post->save();
	    return redirect('/posts')->with('success','Post Ceated');
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $posts = Post::find($id);
		return view('posts.show')->with('posts',$posts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = Post::find($id);
		return view('posts.edit')->with('posts',$posts);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
		
        $this->validate($request, [
        'titel' => 'required',
        'body' => 'required'
    ]);
		$post = Post::find($id);
		$post->titel = $request->get('titel');
		$post->body = $request->get('body');
    	$post->save();
	return redirect('/home')->with('success','Post Updated');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Post = Post::findOrFail($id);

        $Post->delete();
    
        //Session::flash('flash_message', 'Post successfully deleted!');
    
        //return redirect()->route('post.index');
        return redirect('/home');
    }
    public function apiTest(Request $request){
        $id = $request->id;
        $posts = \DB::table('posts')
        ->join('users', 'posts.user_id', '=', 'users.id')
        ->select('posts.titel','posts.body')
        ->where('posts.id', $request->id)
        ->get();
		return $posts;

    }
    public function apiTest1(){
       
        $posts = \DB::table('posts')
        ->join('users', 'posts.user_id', '=', 'users.id')
        ->select('posts.titel','posts.body')
        ->where('posts.id', $request->id)
        ->get();
		return $posts;

    }
    public function add(Request $request)
    {
       
        $post = new Post;
		$post->titel = $request->input('titel');
		$post->body = $request->input('body');
       // $post->user_id = auth()->id;
        $post->user_id = $request->input('user_id');;
        $post->save();
        $insertedId = $post->id;

       
	    return ("add successfully".$insertedId );
    }
    public function delete($id)
    {
        $Post = Post::findOrFail($id);

        $Post->delete();
    
        
        return ("delete successfully");
    }
}
