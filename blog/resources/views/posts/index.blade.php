@extends('layouts.app')
@section('content')

<h1>Post </h1>
@if(count($posts)>0)
	@foreach($posts as $post)
		<div class="well">
			<h3><a href="/laravel/blog/public/posts/{{$post->id}}"> {{$post->titel}}</a></h3>
			<small>Witten on {{$post->created_at}}</small>
			<small>Witten by {{$post->name}}</small>
		</div>
	@endforeach
	{{$posts->links()}}
@else
	<p>No Posts Found</p>
@endif
@endsection