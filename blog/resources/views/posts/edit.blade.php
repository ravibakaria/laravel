@extends('layouts.app')
@section('content')

<h1>Create Post</h1>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!--<form action="{{URL::to('update/'.$posts->id)}}" method="POST">
<form method="POST" action="{{action('PostsController@update', $posts->id)}}">
 @csrf
  <div class="form-group">
    <label>Title</label>
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="text" class="form-control" name="titel" value="{{$posts->titel}}">
  </div>
  <div class="form-group">
    <label>Body</label>
    <textarea id="article-ckeditor" class="form-control" rows="5" name="body" >{{$posts->body}}</textarea>
  </div>
  
  <button type="submit" class="btn btn-default">Submit</button>
</form>-->
{!! Form::open(['action'=>['PostsController@update',$posts->id],'method'=>'POST']) !!}
    <div class ="form-group">
	{{Form::label('titel', 'Title')}}
	{{Form::text('titel',$posts->titel,['class'=>'form-control','placeholder'=>'titel'])}}
	</div>
	<div class ="form-group">
	{{Form::label('body', 'body')}}
	{{Form::textarea('body',$posts->body,['id'=>'article-ckeditor','class'=>'form-control','placeholder'=>'body Text'])}}
	{{Form::hidden('_method', 'PUT')}}
	</div>
	{{Form::submit('Update', ['class'=>'btn btn-primary'])}}
{!! Form::close() !!}

@endsection