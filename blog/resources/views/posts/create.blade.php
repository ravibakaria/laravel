@extends('layouts.app')
@section('content')

<h1>Create Post</h1>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{!! Form::open(['action'=>'PostsController@store','method'=>'POST']) !!}
    <div class ="form-group">
	{{Form::label('titel', 'Title')}}
	{{Form::text('titel','',['class'=>'form-control','placeholder'=>'titel'])}}
	</div>
	<div class ="form-group">
	{{Form::label('body', 'body')}}
	{{Form::textarea('body','',['id'=>'article-ckeditor','class'=>'form-control','placeholder'=>'body Text'])}}
	</div>
	{{Form::submit('submit', ['class'=>'btn btn-primary'])}}
{!! Form::close() !!}

@endsection