@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default text-center">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
					
                    <a href="{{URL::to('/posts/create')}}" class="cta ga-hero-cta"><button>Create your blog</button></a>
                    </br>
                    </br>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                           @if(count($posts)>0)     
                                @foreach($posts as $post)
                                <tr>
                                    <td>{{$post->titel}}</td>
                                    <td><a href="/laravel/blog/public/posts/{{$post->id}}/edit" class="btn btn-primary">Edit post</a></td>
                                    <td>
                                        {!! Form::open([
                                        'method' => 'DELETE',
                                            
                                        ]) !!}
                                            {!! Form::submit('Delete this post?', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                    <h3>No Posts<h3>
                                @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
