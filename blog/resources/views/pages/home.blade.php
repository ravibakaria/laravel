@extends('layouts.app')
@section('content')

<div class="jumbotron text-center" >

<header class="hero--header">
    <h2>Publish your passions, your way</h2>
    <p>Create a unique and beautiful blog. It’s easy and free.</p>
    <a href="{{URL::to('/home')}}" class="cta ga-hero-cta"><button>Create your blog</button></a>
  </header>
  <div>
 @endsection