<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', function (Request $request) {
    return "Mein aa gaya" . $request->firstname;
});
Route::get('/sqr', function (Request $request) {
        $num = $request->number;
        $sqr = $num * $num;
        return "The square of numbers: ".$sqr;
});
Route::post('/cube', function (Request $request) {
    $num = $request->number;
    $cube = $num * $num *$num;
    return "The cube of numbers: ".$cube;
});
Route::get('/getdata', function () {
   $person= [
       "name"=>"ravi",
       "age"=>26,
       "city"=>"mumbai"
   ];
   return $person;
});
Route::get('/posts/{id}', 'PostsController@apiTest');
Route::get('/posts', 'PostsController@apiTest1');
Route::post('/posts/create', 'PostsController@add');
Route::delete('/posts/{id}', 'PostsController@delete');

